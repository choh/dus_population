// Licensed under the EUPL 
/* global d3 */
const population_plot = async () => {
    // Access data
    const data = await d3.json("data/stadtteile_2017_r.geojson")
    const population = await d3.json("data/population.json")
    const annotations = await d3.json("data/population_annotations.json")

    const quarter_name_accessor = d => d.properties.Name
    const quarter_id_accessor = d => d.properties.Stadtteil
    const population_accessor = (d) => {
        const quarter = quarter_name_accessor(d)
        return population[quarter]
    }
    const y_accessor = d => d.population
    const x_accessor = d => d.year

    const year_range = d3.extent(
        population[Object.keys(population)[0]], x_accessor)
    const year_span = year_range[1] - year_range[0] + 1
    const data_years = [...Array(year_span).keys()].map(x => x + 1990)

    // Set dimension
    // ensure map is square
    const smaller_dim = window.innerWidth < window.innerHeight 
        ? window.innerWidth
        : window.innerHeight

    const dimensions = {
        width: smaller_dim * 0.6, 
        height: smaller_dim * 0.8,
        info_width: smaller_dim * 0.4,
        info_height: smaller_dim * 0.3,
        margin: {
            top: 30,
            left: 30,
            bottom: 20,
            right: 20,
            info_padding: 25
        }
    }

    dimensions.bounded_width = dimensions.width - dimensions.margin.left 
        - dimensions.margin.right
    dimensions.bounded_height = dimensions.height - dimensions.margin.top 
        - dimensions.margin.bottom
    dimensions.info_bounded_height = dimensions.info_height 
        - dimensions.margin.info_padding
    dimensions.info_bounded_width = dimensions.info_width
        - dimensions.margin.info_padding

    // draw canvas
    d3.select("body").style("max-height", `${window.innerHeight - 20}px`)

    const wrapper = d3.select("#wrapper")
        .append("svg")
        .attr("height", dimensions.bounded_height)
        .attr("width", dimensions.bounded_width)

    const bounds = wrapper.append("g")
        .style("transform", `translate(
      ${dimensions.margin.left}px, 
      ${dimensions.margin.top}px)`)

    const infobox = d3.select("#infobox")
        .style("transform", `translate(${
            dimensions.width 
        }px, ${
            -dimensions.height / 2 - 2 * dimensions.margin.top
        }px)`)
        .style("max-width", `${dimensions.info_width}px`)
    
    const population_chart_outer = infobox.insert("svg", "#info-annotation")
        .attr("height", dimensions.info_bounded_height)
        .attr("width", dimensions.info_bounded_width)
      
    const population_chart = population_chart_outer.append("g")
     
    // create scales
    const dus_projection = d3.geoMercator()
        .fitExtent([
            [dimensions.margin.left, dimensions.margin.top], 
            [dimensions.bounded_width - dimensions.margin.left, 
                dimensions.bounded_height - dimensions.margin.top]], 
        data)
    const path_generator = d3.geoPath().projection(dus_projection)

    // infobox
    const info_bottom = dimensions.info_bounded_height 
      - dimensions.margin.info_padding
    const info_right = dimensions.info_bounded_width - dimensions.margin.info_padding
    const line_x_scale = d3.scaleLinear()
        .range([0 + 2 * dimensions.margin.info_padding, info_right])
        .domain(year_range)
        .nice()
    const x_axis = d3.axisBottom(line_x_scale)
        .tickFormat(x => d3.format("d")(x))
    const line_x_axis = population_chart.append("g")
        .attr("id", "x-axis")
        .attr("style", 
            `transform:translate(0, ${info_bottom}px)`)
        .call(x_axis)
        
    line_x_axis.append("text")
        .html("Year")
        .attr("fill", "black")
        .style("font-size", "1.4em")
        .style("transform", 
            `translate(
                ${info_right / 2 + dimensions.margin.info_padding}px, 
                50px)`)
        .style("text-anchor", "center")
  
    const line_y_scale = d3.scaleLinear()
        .range([info_bottom, 0 + dimensions.margin.info_padding])
    const line_y_axis = population_chart.append("g")
        .attr("id", "y-axis")
        .style("transform", 
            `translateX(${dimensions.margin.info_padding * 2}px)`)
        .call(d3.axisLeft(line_y_scale))
    line_y_axis.append("text")
        .html("Population")
        .attr("fill", "black")
        .style("font-size", "1.4em")
        .style("text-anchor", "center")
        .style("transform", `rotate(270deg) translate(
            ${-info_bottom / 2 + dimensions.margin.info_padding}px, -50px
        )`)

    // draw data
    const geo_features = bounds.selectAll(".quarter")
        .data(data.features)
        .enter().append("path")
        .attr("stroke", "black")
        .attr("class", "quarter")
        .attr("d", d => path_generator(d))
  
    // draw placeholders
    const population_line = population_chart.append("path")
    population_chart.selectAll("circle")
        .data(data_years).enter()
        .append("circle")

    d3.select("#info-text")
        .style("transform", `translate(${
            dimensions.width + dimensions.margin.info_padding * 2 + 10
        }px, ${
            -dimensions.height / 2
        }px)`)

    // set up interaction
    let show_quarter = 0
    let prev_event = undefined
    let first_click = true

    const do_on_click = (e, datum) => {
        const clicked_quarter = quarter_id_accessor(datum)
        if (show_quarter !== clicked_quarter) {
            show_quarter = clicked_quarter
            const quarter_name = quarter_name_accessor(datum)

            d3.select("#info-text").style("opacity", 0)

            d3.select(prev_event)
                .style("fill", null)
            d3.select(e.target)
                .style("fill", "#a02020")
            d3.select("#quartername").text(quarter_name)
            if (quarter_name in annotations) {
                d3.select("#info-annotation").text(annotations[quarter_name][0])
            } else {
                d3.select("#info-annotation").text(null)
            }

            const population_data = population_accessor(datum)
            const my_locale = {
                "decimal": ".",
                "thousands": " ",
                "grouping": [3],
                "currency": ["", "€"],
            }
            const my_locale_formatter = d3.formatLocale(my_locale)
            line_y_scale.domain(
                [0, d3.max(population_data.map(d => y_accessor(d)))])
                .nice()
            line_y_axis.call(
                d3.axisLeft(line_y_scale)
                    .tickFormat(x => my_locale_formatter.format(",d")(x))
            )

            const transition_duration = first_click ? 0 : 200
            const lineGenerator = d3.line()
                .defined(d => y_accessor(d) !== null)
                .x(d => line_x_scale(x_accessor(d)))
                .y(d => line_y_scale(y_accessor(d)))
            const path = lineGenerator(population_data)

            population_line
                .transition().duration(transition_duration)
                .attr("d", path)
                .attr("fill", "none")
                .attr("stroke", "black")
                .attr("id", "info_line")

            population_chart.selectAll("circle")
                .data(population_data)
                .transition().duration(transition_duration)
                .attr("r", 3)
                .attr("cx", d => line_x_scale(x_accessor(d)))
                .attr("cy", d => line_y_scale(y_accessor(d)))
                .attr("fill", "black")
                .attr("class", d => y_accessor(d) !== null
                    ? "info_point"
                    : "info_missing")
                    
            infobox.style("opacity", 1)
        } else {
            d3.select(e.target)
                .style("fill", null)
            show_quarter = 0
            infobox.style("opacity", 0)
            d3.select("#info-text").style("opacity", 1)
        }
        prev_event = e.target
        first_click = false
    }

    geo_features.on("click", do_on_click)
}

population_plot()