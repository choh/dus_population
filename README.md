# Population in Düsseldorf

This project is an interactive visualisation of the population in the city of Düsseldorf, Germany from 1990–2020. 

Population data and geodata was retrieved from [Open Data Düsseldorf](https://opendata.duesseldorf.de/) and is available under the [Data licence zero - Germany 2.0](https://www.govdata.de/dl-de/zero-2-0).
That data is in the directories `data` and `data-raw`.

For DOM manipulation and enabling interactions this project uses the library `d3.js`. 
It is in the directory `d3` and available as is as specified in the file `d3/LICENSE`.

All other code, i.e. the files for the visualisation as well as the code for processing the raw data are available under the European Union Public Licence, see file LICENCE. 

## Viewing the visualisation

To view and explore the interactive visualisation clone this repository and run a local web server in its directory.
Tools like [`live-server`](https://www.npmjs.com/package/live-server) and Python's [`http.server`](https://docs.python.org/3/library/http.server.html) are suitable for this purpose.
